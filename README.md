## Описание ##
abstract_service - это абстрактный сервис, который позволяет пользователям отправлять задачи другому серверу
([пример реализации](https://bitbucket.org/hanging_in_the_balance/mq_server/overview)) и получать результат выполнения.


## Установка ##
Установите [postgresql](http://www.postgresql.org/download/) и [python2.7+](https://www.python.org/downloads/).
Для работы вам нужно создать базу данных и пользователя для проекта. Прочитать о том, как это сделать, вы можете [здесь](http://eax.me/postgresql-install/).

После этого склонируйте репозиторий, установите зависимости в virtualenv и создайте таблицы:
```
#!bash
$ virtualenv abstract_service_venv --no-site-packages  # создаем виртуальное окружение
$ source abstract_service_venv/bin/activate  # активируем виртуальное окружение

$ git clone git@bitbucket.org:hanging_in_the_balance/abstract_service.git
$ cd abstract_service
$ pip install -r requirements/base.txt  # устанавливаем зависимости

$ python abstract_service/manage.py syncdb
$ python abstract_service/manage.py migrate

```
Не забудьте указать в настройках (abstract_service/abstract_service/settings/local.py) правильные параметры для СУБД и RabbitMQ.

Теперь мы можем запустить сервис:
```
#!bash
$ python abstract_service/manage.py runserver 127.0.0.1:8000 --settings=abstract_service.settings.local

```


## Система администрирования ##
Для управления пользователями и задачами используется админка Django. Чтобы туда попасть, нужно перейти по следующей ссылке:

http://127.0.0.1:8000/admin/



## Получение ключа ##
Перед тем, как мы сможем создавать задания, нам нужно авторизоваться. Сделать это можем так:

http://127.0.0.1:8000/accounts/access_token/?email=john@galt.com&password=whoisjohngalt

Если логин и пароль введены правильно, то в ответ мы получим json-строку со статусом и ключом:
```
#!javascript
{"status": "success", "access_token": "52cd4393-aab2-4073-824d-8d0ef8056ed3"}
```

## Ошибки ##
Если мы совершим ошибку в запросе, в ответ мы получим json-строку с описанием ошибки:
```
#!javascript
{"status": "error", "message": "wrong email or password"}
```


## Создание заданий ##
Чтобы создать задание, мы должны отправить POST-запрос с access_token и msg (допускается передача нескольких msg) по следующему адресу:

http://127.0.0.1:8000/tasks/

Сделать это легко с помощью curl:
```
#!bash
curl -d 'access_token=52cd4393-aab2-4073-824d-8d0ef8056ed3&msg=echo:hey&msg=fibonacci:10' 'http://127.0.0.1:8000/tasks/'
```

Формат msg следующий: 'команда:передаваемый_параметр', где 'команда' == echo|sleep|fibonacci.

Примеры: 'echo:hello', 'fibonacci:500', 'sleep:5'


Если access_token и msg введены правильно, то в ответ мы получим json-строку со статусом и идентификатором задания:
```
#!javascript
{"status": "success", "task_id": 47}
```


## Получение статуса ##
Статус задания можем проверить либо зайдя в админку, либо отправив GET-запрос по следующему адресу:

http://127.0.0.1:8000/tasks/<task_id>/status/

Не забываем при этом передавать access_token. Пример:

http://127.0.0.1:8000/tasks/47/status/?access_token=52cd4393-aab2-4073-824d-8d0ef8056ed3


Если task_id и access_token введены правильно, то мы получим json-строку со статусом выполнения задачи:
```
#!javascript
{"status": "wait"}
```


## Получение результата ##
Результат проверяется похожим образом:

http://127.0.0.1:8000/tasks/<task_id>/result/

Пример:

http://127.0.0.1:8000/tasks/47/result/?access_token=52cd4393-aab2-4073-824d-8d0ef8056ed3


В ответ мы получим json-строку с результатом:
```
#!javascript
{"status": "success", "result": "hey; 55"}
```
где в result будут храниться результаты вычисления переданных сообщений, разделенные точкой с запятой.
