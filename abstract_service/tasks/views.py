from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.http import require_POST

from .models import Task, Message, STATUS_WAIT
from accounts.models import Account
from helpers.decorators.http import json_view
from helpers.exceptions import APIException


@json_view
@require_POST
def create_task(request):
    messages = request.POST.getlist('msg')
    if not messages:
        raise APIException('msg is undefined')
    account = Account.get_by_access_token(_get_access_token(request))
    task_id = Task.create_by_messages(account, messages)
    return {'status': 'success', 'task_id': task_id}


@json_view
def get_status(request, task_id):
    access_token = _get_access_token(request)
    task = _get_task_for_account(task_id, access_token)
    return {'status': task.status}


@json_view
def get_result(request, task_id):
    access_token = _get_access_token(request)
    task = _get_task_for_account(task_id, access_token)
    if task.status == STATUS_WAIT:
        raise APIException('wait')
    return {'status': task.status, 'result': task.result}


@json_view
@require_POST
def update_task_message(request, message_id):
    try:
        status, result = request.POST['status'], request.POST['result']
    except IndexError:
        raise APIException('wrong request')
    message = Message.objects.get(pk=message_id)
    message.status = status
    message.result = result
    message.save()
    return {'status': 'success'}


def _get_access_token(request):
    try:
        return request.REQUEST['access_token']
    except (IndexError, KeyError):
        raise APIException('access_token is undefined')


def _get_task_for_account(task_id, access_token):
    account = Account.get_by_access_token(access_token)
    try:
        task = Task.objects.get(pk=task_id)
        if not task.account_id == account.pk:
            raise APIException("not allowed")
        return task
    except ObjectDoesNotExist:
        raise APIException("task doesn't exist")
