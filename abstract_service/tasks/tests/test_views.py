import json

from django.test import TestCase
from django.test.client import Client

from accounts.models import Account
from tasks.models import Task, Message


class TasksViewsTests(TestCase):
    def setUp(self):
        self.c = Client()
        self.account = Account(email='some_account@gmail.com')
        self.password = 'some_password'
        self.account.set_password(self.password)
        self.account.save()

    def test_create_task(self):
        response = self.c.post('/tasks/')
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data.get('status'), 'error')

        response = self.c.post('/tasks/', {'msg': 'echo:hey'})
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'error')

        access_token = self._get_access_token()
        response = self.c.post('/tasks/', {'msg': 'echo:hey', 'access_token': access_token})
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'success')
        self.assertIn('task_id', data)

        response = self.c.post('/tasks/', {'msg': ['echo:hey', 'echo:hi'], 'access_token': access_token})
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'success')
        self.assertIn('task_id', data)

    def test_status_task(self):
        access_token = self._get_access_token()
        response = self.c.post('/tasks/', {'msg': 'echo:hey', 'access_token': access_token})
        task_id = json.loads(response.content)['task_id']

        response = self.c.get('/tasks/{}/status/'.format(task_id))
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'error')

        response = self.c.get('/tasks/{}/status/'.format(task_id), {'access_token': access_token})
        data = json.loads(response.content)
        self.assertIn(data.get('status'), ['wait', 'success'])

    def test_result_task(self):
        access_token = self._get_access_token()
        task = self._get_task()

        response = self.c.get('/tasks/{}/result/'.format(task.pk))
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'error')

        response = self.c.get('/tasks/{}/result/'.format(task.pk), {'access_token': access_token})
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'success')
        self.assertIn('result', data)

    def _get_access_token(self):
        account = self.account
        response = self.c.get('/accounts/access_token/', {'email': account.email, 'password': self.password})
        data = json.loads(response.content)
        return data['access_token']

    def _get_task(self):
        account = self.account
        task = Task(account=account)
        task.save()
        message = Message(task=task, content='echo:hey', status='success', result='hey')
        message.save()
        return task
