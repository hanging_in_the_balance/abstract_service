# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.mail import EmailMessage
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import Account
from helpers.mq import send_message_to_queue


STATUS_WAIT = 'wait'
STATUS_SUCCESS = 'success'
STATUS_ERROR = 'error'


class Task(models.Model):
    account = models.ForeignKey(Account, verbose_name=u'Пользователь')
    created_at = models.DateTimeField(u'Дата создания', auto_now_add=True)

    @property
    def status(self):
        if self.has_error_messages():
            return STATUS_ERROR
        if self.has_unfinished_messages():
            return STATUS_WAIT
        return STATUS_SUCCESS

    @property
    def result(self):
        if self.has_error_messages() or self.has_unfinished_messages():
            return STATUS_ERROR
        return u'; '.join([m.result for m in self.message_set.all()])

    class Meta:
        verbose_name = u'задание'
        verbose_name_plural = u'задания'
        ordering = ['-created_at']

    def __unicode__(self):
        return u'Задание №{}'.format(self.pk)

    def has_unfinished_messages(self):
        return self.message_set.filter(status=STATUS_WAIT).exists()

    def has_error_messages(self):
        return self.message_set.filter(status=STATUS_ERROR).exists()

    def is_success(self):
        return self.status == STATUS_SUCCESS

    @classmethod
    def create_by_messages(cls, account, messages):
        """
        >>> task_id = Task.create_by_messages(Account.objects.get(1), ['sleep:5', 'sleep:10'])
        """
        task = cls(account=account)
        task.save()
        for content in messages:
            Message(content=content, task=task).save()
        return task.pk


class Message(models.Model):
    STATUS_CHOICES = [
        (STATUS_WAIT, 'wait'),
        (STATUS_SUCCESS, 'success'),
        (STATUS_ERROR, 'error')
    ]

    task = models.ForeignKey(Task, verbose_name=u'Задание')
    content = models.TextField(u'Текст')
    result = models.CharField(u'Результат', max_length=250)
    status = models.CharField(u'Статус', max_length=20,
                              choices=STATUS_CHOICES, default=STATUS_WAIT)

    class Meta:
        verbose_name = u'сообщение'
        verbose_name_plural = u'сообщения'

    def __unicode__(self):
        return u'{} = {} ({})'.format(self.content, self.result,
                                      self.status)


@receiver(post_save, sender=Message)
def send_message_to_queue_if_created(instance, **kwargs):
    if kwargs['created']:
        send_message_to_queue('{}:{}'.format(instance.pk, instance.content))


@receiver(post_save, sender=Message)
def notify_account_if_task_is_complete(instance, **kwargs):
    if not kwargs['created']:
        account = instance.task.account
        task = instance.task
        if account.receive_email_notifications and task.is_success():
            msg = EmailMessage(
                u'Ваша задача выполнена',
                u'Здравствуйте! Задача №{} выполнена. Результат: {}'.format(task.pk, task.result),
                settings.EMAIL_HOST_USER,
                [account.email]
            )
            msg.send(fail_silently=True)
