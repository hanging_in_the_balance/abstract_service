# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Task, Message


class MessageInline(admin.TabularInline):
    model = Message


class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'account', 'status', 'result', 'created_at',)
    list_filter = ('created_at',)
    ordering = ('-created_at',)
    inlines = [MessageInline]
admin.site.register(Task, TaskAdmin)