from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^$', 'tasks.views.create_task', name='create'),
    url(r'^message/(?P<message_id>\d+)/$', 'tasks.views.update_task_message',
        name='update_message'),
    url(r'^(?P<task_id>\d+)/status/$', 'tasks.views.get_status', name='status'),
    url(r'^(?P<task_id>\d+)/result/$', 'tasks.views.get_result', name='result'),
)
