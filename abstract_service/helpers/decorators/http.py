import json

from django.shortcuts import HttpResponse

from helpers.exceptions import APIException


def json_view(view=None, catch_api_exceptions=True):
    def decorated(view):
        def wrapper(request, *args, **kwargs):
            try:
                data = view(request, *args, **kwargs)
                if isinstance(data, HttpResponse):
                    return data
            except APIException as e:
                if catch_api_exceptions:
                    data = {
                        'status': 'error',
                        'message': unicode(e)
                    }
                else:
                    raise e
            return HttpResponse(json.dumps(data), content_type='application/json')
        return wrapper
    if view is None:
        def decorator(view):
            return decorated(view)
        return decorator
    return decorated(view)
