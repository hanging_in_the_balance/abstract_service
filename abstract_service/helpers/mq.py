from django.conf import settings

import pika


DELIVERY_MODE_PERSISTENT = 2


def send_message_to_queue(message):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(**settings.RABBITMQ_CONNECTION_PROPERTIES)
    )
    channel = connection.channel()
    channel.queue_declare(queue=settings.QUEUE_NAME, durable=True)
    channel.basic_publish(
        exchange='',
        routing_key=settings.QUEUE_ROUTING_KEY,
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=DELIVERY_MODE_PERSISTENT
        )
    )
    connection.close()
