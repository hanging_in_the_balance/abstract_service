from .base import *


DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'abstract_service',
        'USER': 'test_user',
        'PASSWORD': 'test_password',
        'HOST': 'localhost'
    }
}

RABBITMQ_CONNECTION_PROPERTIES = {
    'host': 'localhost',
    'port': 5672
}


EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
EMAIL_HOST = 'smtp.mail.ru'
EMAIL_PORT = 2525
EMAIL_HOST_USER = 'abstractservice@mail.ru'
EMAIL_HOST_PASSWORD = 'abstractservice1'
EMAIL_USE_TLS = True