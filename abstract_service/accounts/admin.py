# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .forms import AccountCreationForm, AccountChangeForm
from .models import Account


class AccountAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password', 'receive_email_notifications')
        }),
    )

    fieldsets = (
        (None, {'fields': ('email', 'password', 'receive_email_notifications')}),
        (_('Permissions'), {'fields': ('is_active', 'is_superuser')}),
    )

    form = AccountChangeForm
    add_form = AccountCreationForm

    list_display = ('email', 'is_superuser')
    list_filter = ('is_superuser',)
    search_fields = ('email',)
    filter_horizontal = []
    ordering = ('email',)
admin.site.register(Account, AccountAdmin)
admin.site.unregister(Group)