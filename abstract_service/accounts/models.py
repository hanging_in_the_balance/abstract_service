# -*- coding: utf-8 -*-
import uuid

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from helpers.exceptions import APIException


class AccountManager(BaseUserManager):
    def _create_user(self, email, password, is_superuser, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_active=True,
                          is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, **extra_fields)


class Account(AbstractBaseUser):
    email = models.EmailField(u'E-mail', unique=True)
    access_token = models.CharField(u'Ключ', unique=True, null=True,
                                    blank=True, max_length=36)
    receive_email_notifications = models.BooleanField(
        u'Получать уведомления по электронной почте', default=False
    )
    is_active = models.BooleanField(u'Активный', default=True, blank=True)
    is_superuser = models.BooleanField(u'Администратор', default=False,
                                       blank=True)

    USERNAME_FIELD = 'email'

    objects = AccountManager()

    class Meta:
        verbose_name = u'пользователь'
        verbose_name_plural = u'пользователи'
        ordering = ('-is_active', 'id',)

    def __unicode__(self):
        return u'{}'.format(self.email)

    @property
    def is_staff(self):
        return self.is_superuser

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser

    def get_short_name(self):
        return self.email

    def generate_access_token(self):
        self.access_token = str(uuid.uuid4())
        self.save()

    @classmethod
    def get_by_access_token(cls, access_token):
        try:
            return cls.objects.get(access_token=access_token)
        except ObjectDoesNotExist:
            raise APIException('access_token is wrong')