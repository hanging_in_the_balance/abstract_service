import json

from django.test import TestCase
from django.test.client import Client

from accounts.models import Account


class AccountsViewsTests(TestCase):
    def test_get_access_token(self):
        c = Client()

        response = c.get('/accounts/access_token/')
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'error')

        account = Account(email='some_account@gmail.com')
        password = 'some_password'
        account.set_password(password)
        account.save()

        response = c.get('/accounts/access_token/', {'email': account.email, 'password': password})
        data = json.loads(response.content)
        self.assertEqual(data.get('status'), 'success')
        self.assertIn('access_token', data)