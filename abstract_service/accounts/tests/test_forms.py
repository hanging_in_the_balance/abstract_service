from django.test import TestCase

from accounts.forms import AccountCreationForm, AccountChangeForm
from accounts.models import Account


class AccountsFormsTests(TestCase):
    def test_creation_form(self):
        form = AccountCreationForm({
            'email': 'somecrazyemail@gmail.com',
        })
        self.assertFalse(form.is_valid())

        form = AccountCreationForm({
            'email': 'somecrazyemail',
        })
        self.assertFalse(form.is_valid())

        form = AccountCreationForm({
            'email': 'somecrazyemail@gmail.com',
            'password': 'somecrazypassword'
        })
        form.is_valid()
        print form.errors
        self.assertTrue(form.is_valid())

    def test_change_form(self):
        account = Account(email='somecrazyemail@gmail.com')
        account.set_password('somecrazypassword')
        account.save()

        form = AccountChangeForm({
            'email': ''
        }, instance=account)
        self.assertFalse(form.is_valid())

        form = AccountChangeForm({
            'email': 'someawesomeemail@gmail.com'
        }, instance=account)
        self.assertTrue(form.is_valid())
