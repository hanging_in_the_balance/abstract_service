from django.contrib.auth import authenticate

from helpers.decorators.http import json_view
from helpers.exceptions import APIException


@json_view
def get_access_token(request):
    email = request.GET.get('email')
    password = request.GET.get('password')
    if not email or not password:
        raise APIException('email or password is undefined')
    account = authenticate(email=email, password=password)
    if not account:
        raise APIException('wrong email or password')
    account.generate_access_token()
    return {
        'status': 'success',
        'access_token': account.access_token
    }