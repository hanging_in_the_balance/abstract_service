from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^access_token/', 'accounts.views.get_access_token', name='access_token'),
)
